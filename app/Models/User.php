<?php

namespace Demo\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class User extends Model {

	/**
	 * @var \Illuminate\Database\Connection
	 */
	protected $connection;

	/**
	 *
	 */
	

	protected $table = 'users';

	protected $primaryKey = 'user_id';

	public $timestamps = false;

	protected $fillable = array('email', 'first_name', 'last_name');

	public function getFullNameAttribute() {
		return $this->first_name . ' ' . $this->last_name;
	}

	public function getAddressInfoAttribute() {
		return $this->address . ' ' . $this->city. ' ' . $this->state;
	}


	public function getProviderInfoByKey( $email, $password)
	{
		return DB::table('users')->where('email', $email)->first();
	}

	

	
}