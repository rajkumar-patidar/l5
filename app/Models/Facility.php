<?php

namespace Demo\Models;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model {

	protected $table = 'facility';

	protected $primaryKey = 'facility_id';

	public $timestamps = false;

	public function getAddressInfoAttribute() {
		return $this->address . ' ' . $this->city. ' ' . $this->state;
	}	

	
}