<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group( array( 'middleware' => [ 'guest' ] ), function () {

	Route::get( '/', array( 'as' => 'login', 'uses' => 'AuthController@index' ) );
	Route::post( '/login', array( 'as' => 'login-handler', 'uses' => 'AuthController@loginHandler' ) );
});

Route::group( [ 'middleware' => [ 'auth-token' ] ], function () {

	Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'HomeController@facility' ]);
	Route::get('/logout', ['as' => 'logout', 'uses' => 'AuthController@logout' ]);

	Route::get('/getFacility', ['as' => 'getFacility', 'uses' => 'HomeController@facilityList' ]);


	Route::get('/test', ['as' => 'dashboard', 'uses' => 'HomeController@test' ]);


} );

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');


