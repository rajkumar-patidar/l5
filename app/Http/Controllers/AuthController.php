<?php

namespace Demo\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Demo\Models\User as User;
use HealthSlate\Http\Requests;
use Validator;
class AuthController extends BaseController
{
    //use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	protected $user;	

	function __construct(User $user) {
		$this->user = $user;
		//$this->facility = $facility;
	}


    public function index()
    {
        return view('login');
    }
	
	public function loginHandler()
    {
       $validator = Validator::make(
            request()->all(), [
            'username' => 'required|email|min:4',
            'password' => 'required|min:4',
        ] );
        if ($validator->passes()) {
            $user = $this->user->getProviderInfoByKey( request( 'username' ), request( 'password' ) );
            if(!empty($user))
            {
                session( [ 'user' => $user ] );
                return redirect()->intended( 'dashboard' );
            }
            else
            {
                return redirect()->route( 'login' )->withInput( request()->except( 'password' ) )->with( 'errorMsg', 'Invalid Login Details.' );
            }           
            
        }

        $messages      = $validator->errors();
        $errorMessages = [ ];
        foreach ($messages->all( '<p>:message</p>' ) as $message) {
            $errorMessages[] = $message;
        }

        return redirect()->route( 'login' )->withInput( request()->except( 'password' ) )->with( 'errorMsg', implode( '', $errorMessages ) );
    }


    public function logout()
    {
        info( 'Processing logout request.' );
        session()->flush();

        return redirect()->route( 'login' );
    }   



}
