<?php

namespace Demo\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Demo\Models\Facility as Facility;
use Demo\Helpers\DataTableServiceProvider;
use Carbon\Carbon;

class HomeController extends BaseController
{
    //use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	protected $dataTableService;

	protected $facility;

	public function __construct( Facility $facility, DataTableServiceProvider $dataTableServiceProvider ) {
		$this->facility          = $facility;
		$this->dataTableService  = $dataTableServiceProvider;
	}

	public function test()
	{

		echo 'dfsd';die;
	}

	
	public function facility()
    {
        $this->data['facility'] = $this->facility->all();
        return view('facility.facility', $this->data);
    }


	public function facilityList()
	{
		$facilities = $this->facility->all();
		$data       = collect();
		foreach ( $facilities as $facility ) {
			$row = [
				'facility_id'             => $facility->facility_id,
				'name'                    => $facility->name,
				'contact_person_name'     => $facility->contact_person_name,
				'address'            	  => $facility->address,
				'is_notification_enabled' => $facility->is_notification_enabled
			];
			$data->push( array_values( $row ) );
		}

		return $this->dataTableService->dataTableResponse( $data);

	}



}
