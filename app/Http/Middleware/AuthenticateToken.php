<?php

namespace Demo\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class AuthenticateToken
{
	
	public function handle( $request, Closure $next )
	{
		$user = session( 'user', false );
		// Check if user token is invalid ?
		if (empty($user)) {
			return redirect()->route( 'login' );
		}
		return $next( $request );
	}

	
}
