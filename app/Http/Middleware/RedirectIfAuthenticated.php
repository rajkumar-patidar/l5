<?php

namespace Demo\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class RedirectIfAuthenticated
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     */
    public function __construct()
    {
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle( $request, Closure $next )
    {
        if (session( 'user', false )) {
            info( 'User already logged in.' );
            return redirect()->route( 'dashboard' )->with( 'flash_notice', 'You are already logged in!' );
        }

        return $next( $request );
    }
}
