<?php
/**
 * User: spatel
 * Date: 08/01/16
 * Time: 1:06 PM
 */

namespace Demo\Helpers;

use Closure;
use Exception;
use GuzzleHttp\Client;
use Log;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;

class RequestHelper {

	/**
	 * @var Client
	 */
	protected $client;

	/**
	 * @param Client $client
	 */
	public function __construct( Client $client ) {
		$this->client = $client;
	}

	/**
	 * makeRequest
	 *      Make API request
	 *
	 * @param $METHOD
	 * @param $url
	 * @param array $data
	 * @param array $header
	 * @param array $onException
	 *
	 * @return Exception|mixed|object
	 */
	public function makeRequest( $METHOD, $url, $data = [ ], $header = [ ], $onException = [ ] ) {

		$requestDataKeyName = ($METHOD == 'GET') ? 'query' : 'json';

		try {
			Log::info( "API Request " . $METHOD . "[" . $url . "]" );
			$time     = microtime( true );
			$response = $this->client->request( $METHOD, $url, [
				$requestDataKeyName => $data,
				'headers'           => $header
			] );
		} catch ( Exception $e ) {
			if ( $onException instanceof Closure ) {
				return call_user_func( $onException, $url, $e );
			}
			$onException = (object) ( empty( $onException ) ? [
				'statusCode' => 0,
				'body'       => [
					'errorDetail' => trans( 'common.internal_server_error' )
				]
			] : $onException );

			return $this->handleException( "API Response " . $METHOD . "[" . $url . "] : Exception ", $e, $onException );
		}

		Log::info( "API Response " . $METHOD . "[" . $url . "] status: " . $response->getStatusCode() . " time: " . round( ( microtime( true ) - $time ) * 1000, 2 ) . " ms" );

		return (object) [ 'statusCode' => $response->getStatusCode(), 'body' => json_decode( $response->getBody() ) ];
	}

	/**
	 * @param $METHOD
	 * @param $base_url
	 * @param array $request_data
	 * @param int $concurrency
	 *
	 * @return array
	 * @throws Exception
	 */
	public function makeAsyncCall($METHOD, $base_url, $request_data = [], $concurrency = 15) {
		$results   = [ ];
		$promise  = null;
		$time     = microtime( true );
		try {

			$requests = function ( $request_data ) {
				foreach ( $request_data as $key => $meta ) {
					yield $key => new Request( $meta->METHOD, $meta->url );
				}
			};

			$pool = new Pool( $this->client, $requests($request_data), [

				'concurrency' => $concurrency,

				'fulfilled'   => function ( ResponseInterface $response, $index ) use (&$results){
					// this is delivered each successful response
					$results[ $index ] = (object) [ 'statusCode' => $response->getStatusCode(), 'body' => json_decode( $response->getBody() ) ];
				},
				'rejected'    => function ( $reason, $index ) use ( &$promise ) {
					// this is delivered each failed request
					Log::error( 'Promise Rejected. key: ' . $index.' Cancelling Pool' );
					if ( $promise && method_exists( $promise, 'cancel' ) ) {
						$promise->cancel();
					}
				},
			] );


			Log::info( "API Request Pool started " . $METHOD . "[" . $base_url . "] Size: ".count($request_data) );
			$time     = microtime( true );
			// Initiate the transfers and create a promise
			$promise = $pool->promise();

			// Force the pool of requests to complete.
			$promise->wait();

		} catch ( Exception $exception ) {
			Log::error( $exception->getCode() . ' ' . $exception->getMessage() );
			return [ 'statusCode' => 0, 'error' => true, 'errorDetail' => trans( 'common.internal_server_error' ) ];
		}
		Log::info( "API Request Pool Response " . $METHOD . "[" . $base_url . "] time: " . round( ( microtime( true ) - $time ) * 1000, 2 ) . " ms" );

		return $results;

	}

	/**
	 * handleException
	 *  log exception and return error response
	 *
	 * @param string $prefixString
	 * @param Exception $exception
	 * @param bool|object $return_response
	 * @param bool $print_trace
	 *
	 * @return Exception|object
	 */
	function handleException( $prefixString = '', Exception $exception, $return_response = false, $print_trace = false ) {

		Log::error( $prefixString . ' ' . $exception->getCode() . ' ' . $exception->getMessage() );
		if ( $print_trace ) {
			Log::error( $exception->getTraceAsString() );
		}
		if ( $return_response ) {
			return $return_response;
		}

		return $exception;
	}
} 