<?php
/**
 * User: spatel
 * Date: 7/4/16
 */

namespace Demo\Helpers;

use HealthSlateAdmin\Models\Healthslate;
use Mail;

class MailServiceHelper {

	/**
	 * @param Healthslate $healthslate
	 */
	public function __construct( Healthslate $healthslate ) {
		$atoz_preferences = $healthslate->get_atoz_preferences();

		if ( empty( $atoz_preferences ) ) {
			logger( 'MailServiceHelper: atoz_preferences Table is empty' );
		}

		$atoz_preferences = collect( $atoz_preferences );
		$host             = $atoz_preferences->where( 'preference_name', 'smtpHost' )->first();
		$emailId          = $atoz_preferences->where( 'preference_name', 'emailId' )->first();
		$username         = $atoz_preferences->where( 'preference_name', 'username' )->first();
		$password         = $atoz_preferences->where( 'preference_name', 'password' )->first();

		if ( ! empty( $host ) && ! empty( $emailId ) && ! empty( $username ) && ! empty( $password ) ) {
			config( [ 'mail.host' => $host->preference_value ] );
			config( [ 'mail.username' => $username->preference_value ] );
			config( [ 'mail.password' => $password->preference_value ] );
			config( [ 'mail.from' => [ 'address' => $emailId->preference_value, 'name' => 'Healthslate' ] ] );
		} else {
			logger( 'MailServiceHelper: missing email configuration in atoz_preferences Table' );
		}
	}

	/**
	 * @param $subject
	 * @param array $body
	 * @param $to
	 * @param array $file
	 *
	 * @return int
	 */
	public function sendEmail($subject, array $body, $to, $file = [] ) {
		info( 'Processing send email request' );
		Mail::send( 'email.common' , $body + [ 'subject' => $subject ], function ( $message ) use ($subject, $to, $file ) {
			$message->to( $to );
			$message->subject( $subject );
			if ( isset( $file['full'] ) && isset( $file['title'] ) ) {
				$message->attach( $file['full'], [ 'as' => $file['title'] ] );
			}
		} );
		$failed = count(Mail::failures());
		info( 'Send email request completed, sent_status: '.($failed == 0 ? 'Y' : 'N') );
		return ($failed == 0 ? 1 : 0);
	}
}