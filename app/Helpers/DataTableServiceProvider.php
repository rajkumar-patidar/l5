<?php
/**
 * User: spatel
 * Date: 25/1/16
 */

namespace Demo\Helpers;

use Illuminate\Database\Query\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use Log;
use Yajra\Datatables\Services\DataTable;

/**
 * Class DataTableServiceProvider
 * @package HealthSlateAdmin\Helpers
 */
class DataTableServiceProvider extends DataTable {

	/**
	 * @var
	 */
	protected $data;

	/**
	 * @param JsonResponse $data
	 *
	 * @return $this
	 */
	public function makeFrom( JsonResponse $data ) {
		$this->data = $data;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function ajax() {
		return $this->data;
	}

	/**
	 *
	 */
	public function query() {

	}

	/**
	 * @param Collection $builder
	 * @param bool $isExportRequest
	 * @param string $export
	 *
	 * @return JsonResponse|mixed
	 */
	public function dataTableResponse( Collection $builder, $isExportRequest = false, $export = 'csv' ) {

		if ( $isExportRequest ) {
			$this->datatables->getRequest()->merge( [ 'length' => - 1 ] );
		}

		$jsonResponse = $this->datatables->usingCollection( $builder )->make( true );

		if ( $isExportRequest ) {
			$this->makeFrom( $jsonResponse );
			// is a internal call?
			if ( request('_store') == 'yes' ) {

				info(request()->getMethod() . "[" . request()->path() . "] Processing store export request, Type: ".$export);
				return $this->store( $export );
			}

			Log::info(request()->getMethod() . "[" . request()->path() . "] Processing download request, Type: ".$export);
			if ( $export == 'csv' ) {
				return $this->csv();
			} elseif ( $export == 'excel' ) {
				return $this->excelExport($builder);
			} elseif ( $export == 'pdf' ) {
				return $this->pdf();
			}
		}

		return $jsonResponse;
	}

	/**
	 * @param string $ext
	 *
	 * @return mixed
	 */
	public function store( $ext = 'csv' ) {
		return new JsonResponse($this->buildExcelFile()->store( $ext, false, true ));
	}

	/**
	 * Custom function to export excel from view file
	 *
	 * @param $data
	 * @param string $view
	 *
	 * @return mixed
	 */
	public function excelExport( $data, $view = 'report.excel-export' ) {

		return app( 'excel' )->create( $this->filename(), function ( $excel ) use ( $data, $view ) {
			$excel->sheet( 'exported-data', function ( $sheet ) use ( $data, $view ) {
				if ( view()->exists( $view ) ) {
					$sheet->loadView( $view );
				} else {
					$sheet->fromArray( $this->getDataForExport() );
				}
			} );
		} )->download( 'xls' );
	}

	/**
	 * @param Builder $builder
	 * @param null|callable $searchFunction
	 * @param null|callable $alterResponseFunction
	 * @param string $export
	 *
	 * @return JsonResponse|mixed
	 */
	public function usingQueryBuilder( Builder $builder, $searchFunction = null, $alterResponseFunction = null, $export = 'csv' ) {
		$isExportRequest = $this->isExportRequest();

		$sendColumnTitle = false;

		// if, is a export request, export all rows.
		if ( $isExportRequest ) {
			$this->datatables->getRequest()->merge( [ 'length' => - 1 ] );
			// for export, we need column names as first row
			$sendColumnTitle = true;
		}

		$builderEngine = $this->datatables->usingQueryBuilder( $builder );

		// Calling This will disable default all column search
		// This will be helpful while having custom filter or query is having Raw select.
		// e.g. https://datatables.yajrabox.com/fluent/custom-filter
		if ( is_callable( $searchFunction ) && $this->datatables->getRequest()->isSearchable() ) {
			$builderEngine = $builderEngine->filter( $searchFunction );
		}

		if(is_callable($alterResponseFunction)){
			call_user_func($alterResponseFunction, $builderEngine);
		}

		// Building DataResponse from DB Query
		// make false => resulting data without `key`
		$jsonResponse = $builderEngine->make( $sendColumnTitle );

		if ( $isExportRequest ) {
			//
			$this->makeFrom( $jsonResponse );

			info( request()->getMethod() . "[" . request()->path() . "] Processing export request, Type: " . $export );
			// is a internal call?
			if ( request( '_store' ) == 'yes' ) {

				return $this->store( $export );
			}
			if ( $export == 'csv' ) {

				return $this->csv();
			} else {
				logger( 'Invalid Export Request' );
			}
		}

		return $jsonResponse;
	}

	/**
	 * @return bool
	 */
	public function isExportRequest() {
		$action = request( 'action', null );

		return ( in_array( $action, [ 'csv' ] ) ? true : false );
	}
}