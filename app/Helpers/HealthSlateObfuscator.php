<?php
/**
 * User: spatel
 * Date: 25/3/16
 */

namespace Demo\Helpers;


use HealthSlateAdmin\Models\Patient;
use HealthSlateAdmin\Models\User;

class HealthSlateObfuscator {

	// Default password : Change1t#
	const DEFAULT_PASSWORD = '$2a$10$ahH6opJ.U68UclyEtClrWeS8Y.SUrxDibl7n6ngdO1Y9Tcw990Rve';

	const MAIL_ID_SUFFIX = '@healthslate.com';

	// Min and Max DOB for random calculation
	const MAX_DOB = '2000-12-31';
	const MIN_DOB = '1980-01-01';

	/**
	 * @param User $user
	 *
	 * @return User
	 */
	public function obfuscateUserObject( User &$user ) {

		$user->first_name   = 'firstName';
		$user->last_name    = 'lastName ' . $user->user_id;
		$user->email        = $user->user_id . self::MAIL_ID_SUFFIX;
		$user->phone        = '+12345678900';
		$user->zip_code     = '123456';
		$user->display_name = 'Display Name ' . $user->user_id;
		$user->address      = $user->user_id . ' Test Address';
		$user->password     = self::DEFAULT_PASSWORD;

		return $user;
	}

	/**
	 * @param Patient $patient
	 *
	 * @return Patient
	 */
	public function obfuscatePatientObject( Patient &$patient ) {
		if ( 1 || ! is_null( $patient->dob ) ) {
			$dob          = mt_rand( strtotime( self::MIN_DOB ), strtotime( self::MAX_DOB ) );
			$patient->dob = date( 'Y-m-d H:i:s', strtotime( 'today', $dob ) );
		}
		$patient->mrn      = 'test' . $patient->patient_id;
		$patient->race     = 'test/test';
		$patient->nickname = 'nickname';

		return $patient;

	}

}