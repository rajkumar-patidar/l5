<?php
/**
 * User: spatel
 * Date: 25/3/16
 */
namespace Demo\Helpers;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class BitType extends Type {

	const BIT = 'bit';

	/**
	 * {@inheritdoc}
	 */
	public function getSQLDeclaration( array $fieldDeclaration, AbstractPlatform $platform ) {
		return $platform->getBooleanTypeDeclarationSQL( $fieldDeclaration );
	}

	public function getName() {
		return self::BIT;
	}

	/**
	 * Converts a value from its PHP representation to its database representation
	 * of this type.
	 *
	 * @param mixed $value The value to convert.
	 * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform The currently used database platform.
	 *
	 * @return mixed The database representation of the value.
	 */
	public function convertToDatabaseValue( $value, AbstractPlatform $platform ) {
		return ( $value ? "b''1" : "b'0'" );
	}

	/**
	 * Converts a value from its database representation to its PHP representation
	 * of this type.
	 *
	 * @param mixed $value The value to convert.
	 * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform The currently used database platform.
	 *
	 * @return mixed The PHP representation of the value.
	 */
	public function convertToPHPValue( $value, AbstractPlatform $platform ) {
		return bit_to_int($value);
	}
}