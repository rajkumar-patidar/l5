<?php
/**
 * User: spatel
 * Date: 2/3/16
 */

namespace Demo\Helpers;

use Doctrine\DBAL\Types\Type;
use Illuminate\Database\Eloquent\Model;
use DB;
use Log;

class SqlExportHelper {

	/**
	 * @var \Doctrine\DBAL\Schema\AbstractSchemaManager
	 */
	private $doctrineSchemaManager;

	/**
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function __construct(){
		$this->doctrineSchemaManager = DB::connection()->getDoctrineSchemaManager();

		if(!Type::hasType('bit')){
			Type::addType('bit', 'HealthSlateAdmin\Helpers\BitType');
			// get currently used platform
			$dbPlatform = $this->doctrineSchemaManager->getDatabasePlatform();
			// interpret MySQL BIT as custom bit
			$dbPlatform->registerDoctrineTypeMapping('bit', 'bit');
		}
	}

	/**
	 * @param Model $tableEloquentRow
	 *
	 * @return string
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function get_export_string(Model $tableEloquentRow){
		$name = config('database.connections.'.config('database.default').'.database');
		$table = $tableEloquentRow->getTable();
		$formatted_table_name = $this->backquoteCompat($table);
		$content = "SET SQL_MODE = \"NO_AUTO_VALUE_ON_ZERO\";\r\nSET time_zone = \"+00:00\";\r\n\r\n\r\n/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;\r\n/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;\r\n/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;\r\n/*!40101 SET NAMES utf8 */;\r\n\r\n-- Database: `" . $name . "`\r\n\r\n";

		$result   = [ ];
		$result[] = $tableEloquentRow->toArray();

		$columns = $this->doctrineSchemaManager->listTableColumns($table);
		$rows_num   = 1;
		$st_counter = 0;
		$fields   = implode( ', ', $this->backquoteCompat(array_keys($columns)) );
		foreach ( $result as $row ) {

			//when started (and every after 100 command cycle):
			if ( $st_counter % 100 == 0 || $st_counter == 0 ) {
				$content .= "\nINSERT INTO " . $formatted_table_name. ' (' . $fields . ') VALUES ';
			}

			$content .= "\n(";
			$insert_row = [];
			foreach($row as $column_name => $value){
				// fetching data-type
				if(isset($columns[$column_name])){
					$type = strtolower($columns[$column_name]->getType());
					/**
					 * @source: https://github.com/phpmyadmin/phpmyadmin/blob/8a5bf72775782306cb42415321e09e5cdc494cf1/libraries/plugins/export/ExportSql.php#L2354
					 */
					if(is_null($value)){
						$insert_row[] = 'NULL';
					}
					else if($type == 'bit'){
						$insert_row[] = "b'" . $this->printableBitValue($value, $columns[$column_name]->getLength()) . "'";
					}
					else{
						// treating as string
						$value = str_replace( "\n", "\\n", addslashes( $value ) );
						$insert_row[] = '"' . $value . '"';
					}
				}

			}
			$content .= implode(',',$insert_row);

			$content .= ")";
			//every after 100 command cycle [or at last line] ....p.s. but should be inserted 1 cycle eariler
			if ( ( ( $st_counter + 1 ) % 100 == 0 && $st_counter != 0 ) || $st_counter + 1 == $rows_num ) {
				$content .= ";";
			} else {
				$content .= ",";
			}
			$st_counter = $st_counter + 1;
		}
		$content .= "\r\n";

		$content .= "\r\n\r\n/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;\r\n/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;\r\n/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;";
		return $content;
	}


	/**
	 * Converts a bit value to printable format;
	 * in MySQL a BIT field can be from 1 to 64 bits so we need this
	 * function because in PHP, decbin() supports only 32 bits
	 * on 32-bit servers
	 * @source: https://github.com/phpmyadmin/phpmyadmin/blob/4dce6ec2db4001c5af5c8dae7c077334c55c145a/libraries/Util.php
	 *
	 * @param integer $value  coming from a BIT field
	 * @param integer $length length
	 *
	 * @return string  the printable value
	 */
	public static function printableBitValue($value, $length)
	{
		// if running on a 64-bit server or the length is safe for decbin()
		if (PHP_INT_SIZE == 8 || $length < 33) {
			$printable = decbin($value);
		} else {
			// FIXME: does not work for the leftmost bit of a 64-bit value
			$i = 0;
			$printable = '';
			while ($value >= pow(2, $i)) {
				++$i;
			}
			if ($i != 0) {
				--$i;
			}
			while ($i >= 0) {
				if ($value - pow(2, $i) < 0) {
					$printable = '0' . $printable;
				} else {
					$printable = '1' . $printable;
					$value = $value - pow(2, $i);
				}
				--$i;
			}
			$printable = strrev($printable);
		}
		$printable = str_pad($printable, $length, '0', STR_PAD_LEFT);
		return $printable;
	}

	/**
	 * Adds backquotes on both sides of a database, table or field name.
	 * in compatibility mode
	 *
	 * @source: https://github.com/phpmyadmin/phpmyadmin/blob/4dce6ec2db4001c5af5c8dae7c077334c55c145a/libraries/Util.php#L1012
	 * example:
	 * <code>
	 * echo backquoteCompat('owner`s db'); // `owner``s db`
	 *
	 * </code>
	 *
	 * @param mixed   $a_name        the database, table or field name to
	 *                               "backquote" or array of it
	 * @param string  $compatibility string compatibility mode (used by dump
	 *                               functions)
	 *
	 * @return mixed the "backquoted" database, table or field name
	 *
	 * @access  public
	 */
	public static function backquoteCompat( $a_name, $compatibility = 'MYSQL' ) {
		if ( is_array( $a_name ) ) {
			foreach ( $a_name as &$data ) {
				$data = self::backquoteCompat( $data, $compatibility );
			}

			return $a_name;
		}
		// @todo add more compatibility cases (ORACLE for example)
		switch ( $compatibility ) {
			case 'MSSQL':
				$quote = '"';
				break;
			default:
				$quote = "`";
				break;
		}
		// '0' is also empty for php :-(
		if ( mb_strlen( $a_name ) && $a_name !== '*' ) {
			return $quote . $a_name . $quote;
		} else {
			return $a_name;
		}
	}
}